
Textfield Statefield, Screenfield, Profilefield;
DropdownList[] TVddl = new DropdownList[NUMOFTVS];
Textfield[] TVfield = new Textfield[NUMOFTVS];
Button[] TVBtn = new Button[NUMOFTVS];
Button[] screenBtn = new Button[NUMOFSCREENS];
DropdownList  Portselector, Profileddl;
Textlabel[] screenLbl = new Textlabel[NUMOFSCREENS];
Textfield[] TVddlbl = new Textfield[NUMOFTVS];
// Create all the GUI controls. 

void createGUI() {
  PFont arial20 = createFont("arial", 20);
  PFont p = createFont("arial", 30);
  cp5.setControlFont(p);

  Statefield = cp5.addTextfield("State")              //State
    .setPosition(50, 10)    
      .setDecimalPrecision(2)
        .setSize(200, 31)
          .lock();
  Statefield.captionLabel().style().marginTop= -8;
  Statefield.captionLabel().setFont(arial20);


  Screenfield = cp5.addTextfield("Screen")            //Screen
    .setPosition(280, 10)    
      .setDecimalPrecision(2)
        .setSize(130, 31)
          .lock();
  Screenfield.captionLabel().style().marginTop= -8;
  Screenfield.captionLabel().setFont(arial20);


  Profilefield = cp5.addTextfield("Profile")  //Profile
    .setPosition(40, 125)    
      .setDecimalPrecision(2)
        .setSize(311, 31)
          .lock();
  Profilefield.captionLabel().style().marginTop= -8;
  Profilefield.captionLabel().setFont(arial20);
  cp5.addButton("SELECT")
    .setPosition(310, 125)
      .setSize(130, 31)
        .setId(23);   


  cp5.addButton("ON")                              //Power Buttons
    .setPosition(25, 225)
      .setSize(75, 75)
        .setId(21)
          .getCaptionLabel().align(CENTER, CENTER);
  cp5.addButton("OFF")
    .setPosition(135, 225)
      .setSize(75, 75)
        .setId(22)
          .getCaptionLabel().align(CENTER, CENTER);
  cp5.addButton("REFRESH")
    .setPosition(245, 225)
      .setSize(185, 75)
        .setId(24)
          .getCaptionLabel().align(CENTER, CENTER);

  int count = 0;                                      //Screen Buttons
  for ( int v = 0; v < 5; v++) {
    for (int h = 0; h < 4; h++) {
      screenBtn[count] = cp5.addButton(str(count+1))
        .setPosition (25+(h*110), 320 + (v*95))
          .setSize(75, 50)
            .setId(count);
      screenBtn[count].getCaptionLabel().align(CENTER, CENTER);
      screenLbl[count] = cp5.addTextlabel("LABEL" + str(count+1))
        .setFont(arial20)
          .setSize(75, 75)
            .setWidth(75)
              .setPosition (24+(h*110), 365 + (v*95));
      screenLbl[count].getValueLabel().align(LEFT, BOTTOM);
      count++;
    }
  }


  Portselector = cp5.addDropdownList("Com Port")  //Port Selection
    .setPosition(40, 210)
      .setSize(400, 31)
        .setBarHeight(30)
          .addItems(Ports)
            .setItemHeight(30)
              .setId(1)  
                .setColorBackground(0xff520001) 
                  .setColorForeground(color(220, 30, 32));
  ;
  Portselector.captionLabel().set("Select Port");
  Portselector.captionLabel().style().marginTop = -3;
  Portselector.captionLabel().style().marginLeft = 1;


  for (int i = 0; i< NUMOFTVS;i++) {                        //TV Buttons 
    TVBtn[i] = cp5.addButton(str(i+31))
      .setPosition ((i*100+40), 65)
        .setSize(75, 50)
          .setColorBackground(color(0, 0, 0, 0))
            .setColorForeground(color(0, 0, 0, 0))
              .setColorActive(color(0, 0, 0, 0))
                .setId(30+i);
    TVBtn[i].captionLabel().hide();

    TVfield[i] = cp5.addTextfield("TV"+ (i+1))
      .setPosition((i*100+40), 65)    
        .setDecimalPrecision(0)
          .setSize(60, 31)
            .lock()
              ;
    TVfield[i].valueLabel().style().marginLeft=-5;          
    TVfield[i].captionLabel().style().marginTop= -8;
    TVfield[i].captionLabel().setFont(arial20);

    TVddl[i] = cp5.addDropdownList("TVacc" + (i+1))
      .setCaptionLabel("TV"+str((i+1)))
        .setPosition((40), 150)
          .setWidth(400)
            .setHeight(700)
              .setColorBackground(color(60, 60, 60, 255))
                .setBarHeight(31)
                  .hideBar()
                    .setItemHeight(31)
                      .setId(i+30)
                        .disableCollapse()
                          ;
    TVddl[i].addItem("Power Off", -1);

    TVddlbl[i] = cp5.addTextfield("TVddlbl" + (i+1))
      .setValue("TV"+str((i+1)))
        .setPosition(40, 119)
          .setSize(60, 31)
            .setColorBackground(color(60, 60, 60, 255))
              .hide()
                ;
    TVddlbl[i].captionLabel().hide();
  }
  Profileddl = cp5.addDropdownList("Profile1")
    .setLabel("Profile")
      .setPosition(39, 156)
        .setWidth(270)
          .setHeight(600)
            .setBarHeight(31)
            .setColorBackground(color(60, 60, 60, 255))
              .hideBar()
                .setItemHeight(35)
                  .setId(2)
                    .disableCollapse()
                      ;
}



void controlEvent(ControlEvent theEvent) {
  println(theEvent);
  if (theEvent.isGroup()) {
    if (theEvent.getGroup().getId() < 30) {//Port Selection  
     switch(theEvent.getGroup().getId()) {
       case(1):
      Portselector.setColorBackground(0xff003652);
      Portselector.setColorForeground(0xff00698c);
      myPort = new Serial(this, Serial.list()[int(theEvent.group().value())], 9600);
      portSet = true;
      break;
      case(2)://profile selection
      props.loadScreenProfile(int(theEvent.group().value()));
      break;
    }
    
    }
    else //Channel Selection
    {
      int temp = (theEvent.getGroup().getId()-30);
      curTV.currentChannel[temp]=int(theEvent.getGroup().getValue());
      TVddlbl[temp].hide();
    }
  } 
  else if (theEvent.isController()) {
    if (theEvent.getController().getId() < 20)//Screen Button
    {
      curState.currentScreen = theEvent.getController().getId();
      curState.resendScreen = true;
    }

    else if (theEvent.getController().getId() >= 30)//TV Button
    {
      int temp =(theEvent.getController().getId()-30);
      for (int i=0;i<NUMOFTVS;i++) {
        if (i!=temp) {
          TVddlbl[i].hide();
          TVddl[i].close();
        }
      }
      if (!TVddl[temp].isOpen()) { //if closed, open it
        TVddlbl[temp].show();        
        TVddl[temp].open();
      }
      else
      {
        TVddlbl[temp].hide(); //else close it
        TVddl[temp].close();
      }
    }
    else {
      switch(theEvent.getController().getId()) {

        case(21): //Power on button
        if (portSet)
          myPort.write("P1 P1 P1 P1");
        break;
        case(22): //Power Off Button
        if (portSet)
          myPort.write("P0 P0 P0 P0");
        break;
        case(23)://Profile Select Button
        //        selectInput("Select Profile", "fileSelected");
        //        break;
        if (!Profileddl.isOpen()) { //if closed, open it
          Profileddl.open();
        }
        else
        {
          Profileddl.close();
        }
        break;
        case(24)://Refresh Button
        curState.refresh(props.roleID);
        break;
      }
    }
  }
}

