public class state {
  float currentState;
  float prevState;
  float failToState;
  int currentScreen = -1;
  int prevScreen = -1;
  boolean resendScreen;
  ArrayList<Float> stateNum;
  ArrayList<Integer> stateScreen;
  ArrayList<Integer> custStateScreen;
  ArrayList<String> screenStateList;
  int currentListPos;
  PApplet parent;
  P5Properties stateList;

  state(PApplet parent, int roleID) {
    this.parent = parent;
    stateNum = new ArrayList<Float>();
    stateScreen = new ArrayList<Integer>();
    custStateScreen = new ArrayList<Integer>();
    refresh(roleID);
  }

  public void refresh(int roleID) {
    stateNum.clear();
    stateScreen.clear();
    custStateScreen.clear();
    XML cuelist = loadXML("cuelist.txt");
    XML[] cues = cuelist.getChildren("Cue");
    for (int i = 0; i < cues.length; i++) {
      XML[] roles = cues[i].getChildren("UserRole");
      for (int j = 0; j < roles.length; j++) {
        if (roles[j].getInt("id")==roleID) {
          XML screens = roles[j].getChild("screen");
          stateNum.add(float((cues[i].getInt("id")))/100);
          //        println(cues[i].getInt("id"));
          stateScreen.add(int(screens.getContent()));
          custStateScreen.add(0);
        }
      }
    }
    if (props.useCustomList)
    {
      try {
        stateList=new P5Properties();
        // load a configuration from a file inside the data folder
        stateList.load(openStream(props.profileFiles[props.curProfileNum]));
        for (int i = 0; i<NUMOFSCREENS;i++)
        {
          //         println(stateList.getProperty(str(i), "0"));
          screenStateList = new ArrayList<String>(Arrays.asList(split(stateList.getProperty("screen" + i + ".states", "0"), ",")));
          for (int count = 0; count < screenStateList.size();count++) {
            int pos = stateNum.lastIndexOf(float(screenStateList.get(count))); 
            if (pos >= 0)custStateScreen.set(pos, i);
            if (pos >= 0){
              println(stateNum.get(pos));
              println(custStateScreen.get(pos));
            }
          }
        }
      }
      catch(IOException e) {
        println("couldn't read statelist file...");
      }
    }
  }
  public void set(float stateReq) {
    if (stateNum.lastIndexOf(stateReq)<0) {
      return;
    }
    failToState = currentState;
    currentState = stateReq;
    currentListPos = stateNum.lastIndexOf(stateReq);
    int screenReq;
    if(props.useCustomList)
      screenReq = custStateScreen.get(currentListPos);
    else
      screenReq = stateScreen.get(currentListPos);

    if (screenReq>0 && screenReq <=NUMOFSCREENS){
      currentScreen = screenReq -1;
      resendScreen = true;
    }

  }

  public void next() {
    failToState = currentState;
    if (currentListPos +1 < stateNum.size())
       currentListPos++;
    println(currentListPos);
    println(stateNum.size());
    currentState = stateNum.get(currentListPos);
    int screenReq;
    if(props.useCustomList)
      screenReq = custStateScreen.get(currentListPos);
    else 
      screenReq = stateScreen.get(currentListPos);
    if (screenReq>0 && screenReq <=NUMOFSCREENS)currentScreen = screenReq -1;
  }
  public void prev() {
    failToState = currentState;
    if (currentListPos >0)
    {
      currentListPos--;
      this.currentState = stateNum.get(currentListPos); 
    int screenReq;
    if(props.useCustomList)
      screenReq = custStateScreen.get(currentListPos);
    else 
      screenReq = stateScreen.get(currentListPos);
      if (screenReq>0 && screenReq <=NUMOFSCREENS)currentScreen = screenReq -1;
    }
  }
  public void failed() {
    currentState = failToState;
    this.set(this.currentState);
    int screenReq;
    if(props.useCustomList)
      screenReq = custStateScreen.get(currentListPos);
    else 
      screenReq = stateScreen.get(currentListPos);
    if (screenReq>0 && screenReq <=NUMOFSCREENS)currentScreen = screenReq -1;
  }

  public void parseStateList() {
    stateNum = new ArrayList();
    stateScreen = new ArrayList();
    XML cuelist = loadXML("cuelist.txt");
    XML[] cues = cuelist.getChildren("Cue");
    for (int i = 0; i < cues.length; i++) {
      XML[] roles = cues[i].getChildren("UserRole");
      for (int j = 0; j < roles.length; j++) {
        if (roles[j].getInt("id")==8) {
          XML screens = roles[j].getChild("screen");
          stateNum.add(float((cues[i].getInt("id")))/100);
          stateScreen.add(int(screens.getContent()));
        }
      }
    }
    for (int i=0;i < stateNum.size();i++) {

      println(stateNum.get(i)+":"+stateScreen.get(i));
    }
  }
  public void updateScreen() {
    for (int i=0;i<4;i++) {
      if (curTV.poweringOn[i]) {
        if (curTV.powTime[i] <= millis()) {
          curTV.poweringOn[i]= false;
          if (curTV.macroRunning[i]<=0)
            TVfield[i].setColorBackground(0xff003652);
          curTV.prevChannel[i] = -2;
          if (curTV.macroRunning[i]>0)
            curTV.time[i] = millis() + curTV.delay[i];
        }
      }

      else if (curTV.macroRunning[i]>0) {
        if (curTV.time[i] <= millis()) {
          if (props.macros.get(curTV.macroRunning[i]-1).hasNextChannel()) {
            curTV.currentChannel[i] = props.macros.get(curTV.macroRunning[i]-1).getNextChannel();          
            curTV.time[i] = millis() + curTV.delay[i];
          }
          else {
            TVfield[i].setColorBackground(0xff003652);
            curTV.macroRunning[i] = 0;
          }
        }
      }
    }
    if (currentScreen != prevScreen || resendScreen) {
      Screenfield.setText(nf(currentScreen+1, 0, 0));
      prevScreen = currentScreen;
      for (int i=0;i<NUMOFTVS;i++) {
        curTV.macroRunning[i] =0;
        if (!curTV.poweringOn[i])
          TVfield[i].setColorBackground(0xff003652);
        if (props.screen[currentScreen][i].charAt(0) == 'M') {
          int tempNum = int(props.screen[currentScreen][i].substring(1));        
          curTV.macroRunning[i]=tempNum;
          TVfield[i].setColorBackground(0xff520001);
          curTV.delay[i] = props.macros.get(tempNum-1).delay;
          curTV.time[i] = millis() + curTV.delay[i];
          curTV.currentChannel[i] = props.macros.get(tempNum-1).getFirstChannel();
        }
        else if (props.screen[currentScreen][i].charAt(0) == 'P') {
          curTV.currentChannel[i] = -1;
        }
        else if (int(props.screen[currentScreen][i]) != 0) {
          curTV.currentChannel[i] = int(props.screen[currentScreen][i]);
          if (resendScreen)
            curTV.resendChannel[i] = true;
        }
      }
      resendScreen = false;
    }
  }
  public void updateState() {
    if (currentState != prevState) {
      Statefield.setText(nf(currentState, 0, 2));
      prevState = currentState;
    }
  }
}

