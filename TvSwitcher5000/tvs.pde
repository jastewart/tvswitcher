public class tv {
  int[] currentChannel = new int[4];
  int[] prevChannel = new int[4];
  int[] macroRunning = new int[4];
  int[] delay = new int[4];
  int[] time = new int[4];
  String[] toSend = new String[4];
  boolean[] resendChannel = new boolean[4];
  String command;
  boolean sendCommand = false;
  boolean[] poweringOn = new boolean[4];
  int[] powTime = new int[4];
  public void updateTVs() {
    for (int i=0;i<=3;i++) {


      if ((currentChannel[i]!=prevChannel[i]|| resendChannel[i])&& !poweringOn[i]) {
        if (prevChannel[i] == -1 && !poweringOn[i]) {
          poweringOn[i] = true;
          TVfield[i].setColorBackground(0xff520001);
          toSend[i] = "P1";  
          sendCommand = true;
          powTime[i] = millis() + POWERONTIME;
        }

        else {

          resendChannel[i] = false;
          prevChannel[i] = currentChannel[i];
          if (currentChannel[i] == -1) {
            TVfield[i].setValue("Off");
            toSend[i] = "P0";
            sendCommand = true;
          }
          else {
            TVfield[i].setText(nf(currentChannel[i], 0, 0));

            toSend[i] = str(currentChannel[i]);
            sendCommand = true;
          }
        }
      }
      else
        toSend[i] = "0";
    }
    command = join(toSend, " ");
    command += " ";
    if (sendCommand)
    {
      sendCommand = false;
      if (portSet) {
        myPort.write(command);
        println(command);
      }
    }
  }
}

