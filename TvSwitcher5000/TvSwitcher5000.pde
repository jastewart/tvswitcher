
import controlP5.*;
import g4p_controls.*;
import java.io.*;
import java.net.*;
import java.util.*;
import processing.serial.*;

import java.sql.*;
int NUMOFTVS = 4;
int NUMOFSCREENS = 20;
int POWERONTIME = 9500;
ControlP5 cp5;

MulticastSocket ms;
DatagramPacket received;
state curState;
tv curTV;
props props;
Serial myPort;
String[] Ports;
boolean portSet = false;
boolean sketchFullScreen(){
 return true;
}

void setup() {
  size(480, 800);
  println("Started");
  Ports = Serial.list();
  cp5 = new ControlP5(this);

  createGUI();
  props = new props();
  curTV = new tv();
  curState = new state(this, props.roleID);
  initMulticast();
  thread("receive");
}
void draw() {
  background(0, 0, 0);
  curState.updateState();
  curState.updateScreen();
  curTV.updateTVs();
  //  if(portSet && myPort.available()>0)
  //    {
  //      while(myPort.available()>0)
  //      {
  //        String inByte = myPort.readString();
  //        print(inByte);
  //      }
  //    }
}


void initMulticast() {

  try {
    InetAddress ip;
    ip=InetAddress.getByName("228.6.1.30");
    ms=new MulticastSocket(20201);
    ms.joinGroup(ip);
    ms.setSoTimeout(500);
  }
  catch(UnknownHostException ex) {
    println(ex);
  }
  catch(IOException ex) {
    println("Multicast init " + ex);
  }
}



void receive() {
  while (true) {
    try {
      byte packetBuffer[] = new byte[806];
      received=new DatagramPacket(packetBuffer, packetBuffer.length);
      ms.receive(received);
      if (props.consoleIP.equals(received.getAddress()))
      {
          int b1 = packetBuffer[801];
          int b2 = packetBuffer[802] * 256;
          int b3 = packetBuffer[803] * 65536;
          int stateNumber = (b1 + b2 + b3) / 100;
          println (int(stateNumber));
          curState.set(int(stateNumber));
      }
    }
    catch(IOException ex) {
    }
  }
}
